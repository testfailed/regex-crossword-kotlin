package de.chagemann.regexcrossword.features.selectcategory

import android.animation.ArgbEvaluator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.RippleDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import de.chagemann.regexcrossword.R
import de.chagemann.regexcrossword.db.CrosswordCategory
import de.chagemann.regexcrossword.extensions.getStringWithResKey

class CategoryAdapter(
        private val context: Context,
        private val crosswordCategoryList: List<CrosswordCategory> = listOf()
) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    var clickListener: MutableLiveData<Int> = MutableLiveData()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_category, parent, false))
    }

    override fun getItemCount(): Int {
        return crosswordCategoryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = crosswordCategoryList[position]

        holder.rootLayout.background = getPressedColorRippleDrawable(position)

        holder.categoryNameTextView.text =
                context.getStringWithResKey(category.levelCategory.nameResKey)

        holder.itemView.setOnClickListener {
            clickListener.value = position
        }

        holder.levelStatsTextView.text = String.format(
            context.getString(R.string.level_stats_template),
            category.levelCompleted,
            category.levelTotal
        )
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rootLayout = view.findViewById(R.id.relativeLayout) as RelativeLayout
        val categoryNameTextView = view.findViewById(R.id.categoryNameTextView) as TextView
        val levelStatsTextView = view.findViewById(R.id.levelStatsTextView) as TextView
    }

    private fun interpolateBackgroundColor(position: Int): Int {
        val fraction =
                (1 / (crosswordCategoryList.size.toFloat() - 1)) * (position.toFloat() + Float.MIN_VALUE)

        return ArgbEvaluator().evaluate(
            fraction,
            ContextCompat.getColor(context, R.color.colorPrimaryLight),
            ContextCompat.getColor(context, R.color.colorPrimary)
        ) as Int
    }

    @SuppressLint("PrivateResource")
    private fun getPressedColorRippleDrawable(position: Int): RippleDrawable {
        val color = interpolateBackgroundColor(position)
        val colorPressed = ContextCompat.getColor(context, androidx.appcompat.R.color.ripple_material_dark)

        val colorStateList = ColorStateList.valueOf(colorPressed)
        return RippleDrawable(colorStateList, ColorDrawable(color), null)
    }
}