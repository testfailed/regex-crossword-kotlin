package de.chagemann.regexcrossword.features.selectlevel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import de.chagemann.regexcrossword.R
import de.chagemann.regexcrossword.db.Crossword

class SelectLevelAdapter(private val context: Context, private val crosswordList: List<Crossword>)
    : RecyclerView.Adapter<SelectLevelAdapter.ViewHolder>() {

    var clickListener: MutableLiveData<Int> = MutableLiveData()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_level, parent, false))
    }

    override fun getItemCount() = crosswordList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val crossword = crosswordList[position]

        holder.apply {
            levelNameTextView.text = crosswordList[position].name

            itemView.setOnClickListener {
                clickListener.value = position
            }

            if (crossword.levelCompleted) {
                finishedIcon.visibility = View.VISIBLE
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val levelNameTextView = view.findViewById(R.id.levelNameTextView) as TextView
        val finishedIcon = view.findViewById(R.id.finishedIcon) as ImageView
    }
}