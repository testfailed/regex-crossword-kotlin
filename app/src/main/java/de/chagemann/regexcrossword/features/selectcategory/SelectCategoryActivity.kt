package de.chagemann.regexcrossword.features.selectcategory

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import de.chagemann.regexcrossword.R
import de.chagemann.regexcrossword.databinding.SelectCategoryActivityBinding
import de.chagemann.regexcrossword.features.selectlevel.SelectLevelActivity

class SelectCategoryActivity : AppCompatActivity() {

    private lateinit var binding: SelectCategoryActivityBinding

    private lateinit var viewModel: CategoryActivityViewModel

    private var options: ActivityOptions? = null

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SelectCategoryActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        binding = SelectCategoryActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        afterViews()
    }

    private fun afterViews() {
        // Override the app name with this string to be used as toolbar title.
        title = getString(R.string.title_activity_category)

        binding.categoryRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.categoryRecyclerView.adapter = CategoryAdapter(this)
        initViewModel()
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this).get(CategoryActivityViewModel::class.java)

        viewModel.crosswordCategoryList.observe(this, { categoryList ->
            val rv = binding.categoryRecyclerView
            rv.swapAdapter(CategoryAdapter(this, categoryList), false)

            (rv.adapter as CategoryAdapter).clickListener.observe(this, Observer clickListener@{ position ->
                val intent =
                        SelectLevelActivity.newIntent(this, categoryList[position].levelCategory)
                options?.let {
                    startActivity(intent, it.toBundle())
                    return@clickListener
                }
                startActivity(intent)
            })

        })
    }
}
