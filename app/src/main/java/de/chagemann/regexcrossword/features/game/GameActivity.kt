package de.chagemann.regexcrossword.features.game

import android.animation.ValueAnimator
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.GridLayout
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.transition.TransitionManager
import com.github.florent37.viewtooltip.ViewTooltip
import de.chagemann.regexcrossword.R
import de.chagemann.regexcrossword.databinding.GameActivityBinding
import de.chagemann.regexcrossword.db.Crossword
import de.chagemann.regexcrossword.extensions.TAG
import de.chagemann.regexcrossword.extensions.fromHtml
import de.chagemann.regexcrossword.extensions.hideKeyboard
import de.chagemann.regexcrossword.features.selectcategory.SelectCategoryActivity
import org.jetbrains.anko.collections.forEachByIndex
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.singleLine
import org.jetbrains.anko.toast

const val TRANSITION_DURATION_SHORT = 150L
const val TRANSITION_DURATION_MEDIUM = 500L
const val TRANSITION_DURATION_LONG = 1000L

class GameActivity : AppCompatActivity() {

    private lateinit var binding: GameActivityBinding

    private lateinit var editTextTable: Array<Array<EditText>>

    private lateinit var viewModel: GameActivityViewModel

    private var tooltips: List<ViewTooltip> = listOf()

    private var unsolvedLevel: Crossword? = null

    companion object {
        fun newIntent(context: Context, levelName: String): Intent {
            return Intent(context, GameActivity::class.java)
                    .putExtra(context.getString(R.string.extra_key_level_name), levelName)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = GameActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        afterViews()
    }

    private fun afterViews() {
        val levelName: String? = intent.getStringExtra(getString(R.string.extra_key_level_name))
        if (levelName.isNullOrBlank()) {
            Log.e(TAG, "LevelName extra is null/empty inside GameActivity")
            toast("The name of this level is empty. Something is wrong with it.")
            finish()
            return
        }
        observeViewModel(levelName)
        binding.verifyCrosswordButton.setOnClickListener {
            validateCrossword()
        }
    }

    private fun observeViewModel(levelName: String) {
        viewModel = ViewModelProvider(
            this,
            GameActivityViewModelFactory(this.application, levelName)
        )[GameActivityViewModel::class.java]

        viewModel.crossword.observe(this, { crossword: Crossword? ->
            when (crossword) {
                null -> {
                    toast("No crossword could be found with the level name '$levelName'.")
                }
                else -> {
                    title = crossword.name
                    if (crossword.levelCompleted) {
                        invalidateOptionsMenu()
                    }
                    if (!::editTextTable.isInitialized) {
                        setupTable(crossword)
                    }
                }
            }
        })

        viewModel.crosswordsForCurrentCategory.observe(this, Observer { crosswordList: List<Crossword>? ->
            crosswordList?.forEachByIndex {
                if (!it.levelCompleted && viewModel.crossword.value != it) {
                    unsolvedLevel = it
                    return@Observer
                }
            }
        })
    }

    /**
     * Setup the table of EditText's, resembling a table of cells.
     * Also sets up ViewTooltips which show the regex rules of the row/column.
     */
    private fun setupTable(crossword: Crossword) {
        val amountCols = crossword.topInstructions.size
        val amountRows = crossword.leftInstructions.size

        binding.gridLayout.columnCount = amountCols
        binding.gridLayout.rowCount = amountRows

        val filterToOneChar = arrayOf(InputFilter.LengthFilter(1), InputFilter.AllCaps())
        val monospaceTypeface = Typeface.MONOSPACE

        @Suppress("RemoveExplicitTypeArguments")
        editTextTable = Array<Array<EditText>>(amountRows) {
            Array(amountCols) {
                EditText(this)
            }
        }

        editTextTable.forEachIndexed { rowIndex, arrayOfEditTexts ->

            arrayOfEditTexts.forEachIndexed { colIndex, editText ->
                with(editText) {
                    typeface = monospaceTypeface
                    filters = filterToOneChar
                    gravity = Gravity.CENTER_HORIZONTAL
                    singleLine = true
                    imeOptions = EditorInfo.IME_ACTION_DONE or EditorInfo.IME_FLAG_NO_EXTRACT_UI
                    setOnEditorActionListener { _, actionId, event ->
                        if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                            validateCrossword()
                            true
                        } else false
                    }

                    addTextChangedListener(SpaceTextWatcher())

                    // TODO extract
                    onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                        if (!hasFocus) return@OnFocusChangeListener
                        tooltips.forEach {
                            it.close()
                        }

                        val tipLeft = ViewTooltip.on(editTextTable[rowIndex][0])
                                .position(ViewTooltip.Position.LEFT)
                                .text(crossword.leftInstructions[rowIndex])
                                .textTypeFace(monospaceTypeface)

                        val tipTop = ViewTooltip.on(editTextTable[0][colIndex])
                                .position(ViewTooltip.Position.TOP)
                                .text(crossword.topInstructions[colIndex])
                                .textTypeFace(monospaceTypeface)

                        tooltips = listOf(tipLeft, tipTop)
                        tooltips.forEach {
                            it.autoHide(false, 0)
                                    .arrowWidth(0)
                                    .color(ContextCompat.getColor(context, R.color.colorPrimary))
                                    .withShadow(false)
                                    .show()
                        }
                    }
                }
                editTextTable[rowIndex][colIndex] = editText
                editText.layoutParams =
                        GridLayout.LayoutParams(GridLayout.spec(rowIndex), GridLayout.spec(colIndex, 1f))
                binding.gridLayout.addView(editText)
            }
        }
        editTextTable[0][0].requestFocus()
    }

    private fun validateCrossword() {
        if (!isCrosswordValid()) {
            animateFABOnInvalidCrossword()
            return
        }

        doAsync {
            viewModel.markLevelAsFinished()
        }

        animateFABonValidCrossword()
        Handler().postDelayed(::showNextDialog, TRANSITION_DURATION_MEDIUM)
    }

    private fun showNextDialog() {
        val crossword = unsolvedLevel
        if (crossword == null) {
            AlertDialog.Builder(this).apply {
                setTitle(getString(R.string.category_finished_dialog_title))
                setMessage(getString(R.string.category_finished_dialog_message))
                setPositiveButton(getString(R.string.category_finished_dialog_positive)) { _, _ ->
                    navigateUpTo(SelectCategoryActivity.newIntent(this@GameActivity))
                }
                show()
            }
            return
        }

        AlertDialog.Builder(this).apply {
            setTitle(getString(R.string.level_finished_dialog_title))
            setMessage(String.format(getString(R.string.level_finished_dialog_message), crossword.name).fromHtml())
            setPositiveButton(getString(R.string.level_finished_dialog_positive)) { _, _ ->
                startActivity(newIntent(this@GameActivity, crossword.name))
                finish()
            }
            show()
        }
    }

    private fun isCrosswordValid(): Boolean {
        return pairsAreValid(constructPairs() ?: return false)
    }

    private fun getCommonValueAnimator(
            @ColorRes startColorId: Int,
            @ColorRes endColorId: Int
    ) = ValueAnimator.ofArgb(
        ContextCompat.getColor(this, startColorId),
        ContextCompat.getColor(this, endColorId)
    ).apply {
        interpolator = AccelerateInterpolator()
        duration = TRANSITION_DURATION_SHORT
    }

    private fun animateFABonValidCrossword() {
        getCommonValueAnimator(R.color.colorPrimary, R.color.colorSuccess).apply {
            addUpdateListener {
                binding.verifyCrosswordButton.backgroundTintList =
                        ColorStateList.valueOf(animatedValue as Int)
            }
            start()
        }

        TransitionManager.beginDelayedTransition(binding.gameRelativeLayout)
        binding.verifyCrosswordButton.text = getString(R.string.fab_verify_valid)
        binding.verifyCrosswordButton.setOnClickListener { showNextDialog() }
        hideKeyboard()
    }

    private fun animateFABOnInvalidCrossword() {
        getCommonValueAnimator(R.color.colorPrimary, R.color.colorDanger).apply {
            addUpdateListener {
                binding.verifyCrosswordButton.backgroundTintList = ColorStateList.valueOf(animatedValue as Int)
            }
            start()
        }

        getCommonValueAnimator(R.color.colorDanger, R.color.colorPrimary).apply {
            addUpdateListener {
                binding.verifyCrosswordButton.backgroundTintList = ColorStateList.valueOf(animatedValue as Int)
            }
            startDelay = TRANSITION_DURATION_LONG - TRANSITION_DURATION_SHORT
            start()
        }

        TransitionManager.beginDelayedTransition(binding.gameRelativeLayout)
        binding.verifyCrosswordButton.text = getString(R.string.fab_verify_invalid)
        binding.verifyCrosswordButton.isEnabled = false
        Handler().postDelayed({
            TransitionManager.beginDelayedTransition(binding.gameRelativeLayout)
            binding.verifyCrosswordButton.text = getString(R.string.fab_verify_crossword)
            binding.verifyCrosswordButton.isEnabled = true
        }, TRANSITION_DURATION_LONG)
    }

    private fun constructPairs(): List<RegexPair>? {
        val crossword = viewModel.crossword.value ?: return null
        val amountCols = crossword.topInstructions.size
        val amountRows = crossword.leftInstructions.size

        val pairs = mutableListOf<RegexPair>()
        val sb = StringBuilder()

        // Row Pairs
        for (y in 0 until amountRows) {
            for (x in 0 until amountCols) {
                sb.append(editTextTable[y][x].text)
            }
            val stringToMatch = sb.toString().replace('␣', ' ')
            pairs.add(RegexPair(crossword.leftInstructions[y], stringToMatch))
            sb.clear()
        }

        // Column Pairs
        for (x in 0 until amountCols) {
            for (y in 0 until amountRows) {
                sb.append(editTextTable[y][x].text)
            }
            val stringToMatch = sb.toString().replace('␣', ' ')
            pairs.add(RegexPair(crossword.topInstructions[x], stringToMatch))
            sb.clear()
        }

        return pairs
    }

    private fun pairsAreValid(regexPairList: List<RegexPair>): Boolean {
        return regexPairList.all(RegexPair::isValid)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.game, menu)
        if (viewModel.crossword.value?.levelCompleted == true) {
            menu.findItem(R.id.item_level_finished).isVisible = true
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.item_level_finished -> {
                showNextDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        finishAfterTransition()
    }
}

internal class SpaceTextWatcher : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
        val editable = s ?: return
        when (editable.singleOrNull()) {
            ' ' -> {
            }
            else -> return
        }
        editable.replace(0, 1, "␣")
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
}
