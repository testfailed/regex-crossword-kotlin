package de.chagemann.regexcrossword.extensions

import android.os.Build
import android.text.Html
import android.text.Spanned

// https://stackoverflow.com/a/45963533/3991578
fun String.fromHtml(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(this)
    }
}